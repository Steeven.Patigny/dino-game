<?php

// creation classe

class personne{

    public float $taille;
    public float $poids;

    // creaction construct

    public function __construct(float $taille,float $poids)
    {
        $this->taille = $taille;
        $this->poids = $poids;
    }

    // fonction get

    public function getTaille():float
    {
    return $this->taille ;
    }
    public function getPoids():float
    {
    return $this->poids ;
    }

    // fonction set

    public function setTaille(float $taille):void
    {
        $this->taille = $taille;
    }
    public function setPoids(float $poids):void
    {
    $this->poids = $poids;
    }

    // calcul imc

    public function imc():float
    {
        $imc = $this->poids/($this->taille*$this->taille);
            echo round($imc,2);
        if ($imc<18){
            echo 'vous etes maigre <br>';
        }else if ($imc<30){ 
            echo 'vous etes de corpulence normale <br>';

        }else {
            echo 'vous etes obese <br>';
        }
        return $imc;
    }
}

$personne = new personne(1.85,80.2);

$personne2 = new personne(1.65,155.8);

$personne3 = new personne(2.10,60.8);

$personne->imc();

$personne2->imc();

$personne3->imc();


