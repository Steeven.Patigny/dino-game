const griff = document.getElementById("griff");
const spike = document.getElementById("spike");

function jump() {
  if (griff.classList != "jump") {
    griff.classList.add("jump");

    setTimeout(function () {
      griff.classList.remove("jump");
    }, 300);
  }
}

let isAlive = setInterval(function () {
  // get current griff Y position
  let griffTop = parseInt(window.getComputedStyle(griff).getPropertyValue("top"));

  // get current spike X position
  let spikeLeft = parseInt(
    window.getComputedStyle(spike).getPropertyValue("left")
  );

  // detect collision
  if (spikeLeft < 50 && spikeLeft > 0 && griffTop >= 140) {
    // collision
    alert("Game Over!");
  }
}, 10);

document.addEventListener("keydown", function (event) {
  jump();
});

let interval = null;
let playerScore = 0;

// Fonction score

let scoreCounter = ()=> {
    playerScore++;
    score.innerHTML = `Score ${playerScore}`;
}
